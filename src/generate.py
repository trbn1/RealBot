# -*- coding: utf-8 -*-

import config as cfg
import multiprocessing
from textgenrnn import textgenrnn
import time

def generate_messages(name):
    name = ''
    cfg.generate_training_config(name)
    config = cfg.load_config(name)

    textgen = textgenrnn(config.get('Model', 'weights'),
                        config.get('Model', 'vocab'),
                        config.get('Model', 'config'))
    generated = textgen.generate(n=int(config.get('Model', 'generated_lines')), temperature=1, return_as_list=True)

    return generated


if __name__ == '__main__':
    name = ''
    pool = multiprocessing.Pool(processes=7)
    generated = pool.map(generate_messages, name)
    pool.close()
    pool.join()

    cfg.generate_training_config(name)
    config = cfg.load_config(name)
    with open(config.get('FilePaths', 'generated_messages'), 'w', encoding='utf8') as f:
        for item in generated:
            for subitem in item:
                if subitem != '':
                    f.write(subitem + '\n')
